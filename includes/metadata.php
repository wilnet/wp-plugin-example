<?php

namespace Yawave;

// Save the field
/**
 * Display Yawave ID in category and tag edit screen, no edit allowed.
 * @param type $term
 */
function yw_display_external_id($term) {
    if (!in_array($term->taxonomy, ['category', 'portal', 'post_tag']))
        return;
    $term_id = $term->term_id;
    $term_meta = get_term_meta($term_id, 'yawave_id', true);
    ?>
    <tr class="form-field">
        <th scope="row">
            <label for="term_meta[featured]"><?php echo _e('Yawave internal ID') ?></label>
        <td>
    <?php echo (!empty($term_meta)) ? $term_meta : "-"; ?>
        </td>
    </th>
    </tr>
    <?php
}

add_action('category_edit_form_fields', 'Yawave\yw_display_external_id');
add_action('edit_tag_form_fields', 'Yawave\yw_display_external_id');



if (!function_exists('publication_post_type')) {

// Register Custom Post Type
    function publication_post_type() {

        $labels = array(
            'name' => _x('Publications', 'Post Type General Name', 'yavawe'),
            'singular_name' => _x('Publication', 'Post Type Singular Name', 'yavawe'),
            'menu_name' => __('Publications', 'yavawe'),
            'name_admin_bar' => __('Publication', 'yavawe'),
            'archives' => __('Item Archives', 'yavawe'),
            'attributes' => __('Item Attributes', 'yavawe'),
            'parent_item_colon' => __('Parent Item:', 'yavawe'),
            'all_items' => __('All Items', 'yavawe'),
            'add_new_item' => __('Add New Item', 'yavawe'),
            'add_new' => __('Add New', 'yavawe'),
            'new_item' => __('New Item', 'yavawe'),
            'edit_item' => __('Edit Item', 'yavawe'),
            'update_item' => __('Update Item', 'yavawe'),
            'view_item' => __('View Item', 'yavawe'),
            'view_items' => __('View Items', 'yavawe'),
            'search_items' => __('Search Item', 'yavawe'),
            'not_found' => __('Not found', 'yavawe'),
            'not_found_in_trash' => __('Not found in Trash', 'yavawe'),
            'featured_image' => __('Featured Image', 'yavawe'),
            'set_featured_image' => __('Set featured image', 'yavawe'),
            'remove_featured_image' => __('Remove featured image', 'yavawe'),
            'use_featured_image' => __('Use as featured image', 'yavawe'),
            'insert_into_item' => __('Insert into item', 'yavawe'),
            'uploaded_to_this_item' => __('Uploaded to this item', 'yavawe'),
            'items_list' => __('Items list', 'yavawe'),
            'items_list_navigation' => __('Items list navigation', 'yavawe'),
            'filter_items_list' => __('Filter items list', 'yavawe'),
        );
        $args = array(
            'label' => __('Publication', 'yavawe'),
            'description' => __('Publication Description', 'yavawe'),
            'labels' => $labels,
            'show_in_rest' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'comments', 'custom-fields'),
            //'supports' => array('editor'),
            'taxonomies' => array('category', 'post_tag', 'portals'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 5,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'post',
        );
        register_post_type('publication', $args);
    }

//add_action( 'init', 'Yawave\publication_post_type', 0 );
}

if (!function_exists('portal_taxonomy')) {

// Register Custom Taxonomy
    function portal_taxonomy() {

        $labels = array(
            'name' => _x('Portals', 'Taxonomy General Name', 'yawave'),
            'singular_name' => _x('Portal', 'Taxonomy Singular Name', 'yawave'),
            'menu_name' => __('Portals', 'yawave'),
            'all_items' => __('All Items', 'yawave'),
            'parent_item' => __('Parent Item', 'yawave'),
            'parent_item_colon' => __('Parent Item:', 'yawave'),
            'new_item_name' => __('New Item Name', 'yawave'),
            'add_new_item' => __('Add New Item', 'yawave'),
            'edit_item' => __('Edit Item', 'yawave'),
            'update_item' => __('Update Item', 'yawave'),
            'view_item' => __('View Item', 'yawave'),
            'separate_items_with_commas' => __('Separate items with commas', 'yawave'),
            'add_or_remove_items' => __('Add or remove items', 'yawave'),
            'choose_from_most_used' => __('Choose from the most used', 'yawave'),
            'popular_items' => __('Popular Items', 'yawave'),
            'search_items' => __('Search Items', 'yawave'),
            'not_found' => __('Not Found', 'yawave'),
            'no_terms' => __('No items', 'yawave'),
            'items_list' => __('Items list', 'yawave'),
            'items_list_navigation' => __('Items list navigation', 'yawave'),
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
        );
        register_taxonomy('portal', array('post'), $args);

        $labels = array(
            'name' => _x('Media Type', 'Taxonomy General Name', 'yawave'),
            'singular_name' => _x('Media Type', 'Taxonomy Singular Name', 'yawave'),
            'menu_name' => __('Media Types', 'yawave'),
            'all_items' => __('All Items', 'yawave'),
            'parent_item' => __('Parent Item', 'yawave'),
            'parent_item_colon' => __('Parent Item:', 'yawave'),
            'new_item_name' => __('New Item Name', 'yawave'),
            'add_new_item' => __('Add New Item', 'yawave'),
            'edit_item' => __('Edit Item', 'yawave'),
            'update_item' => __('Update Item', 'yawave'),
            'view_item' => __('View Item', 'yawave'),
            'separate_items_with_commas' => __('Separate items with commas', 'yawave'),
            'add_or_remove_items' => __('Add or remove items', 'yawave'),
            'choose_from_most_used' => __('Choose from the most used', 'yawave'),
            'popular_items' => __('Popular Items', 'yawave'),
            'search_items' => __('Search Items', 'yawave'),
            'not_found' => __('Not Found', 'yawave'),
            'no_terms' => __('No items', 'yawave'),
            'items_list' => __('Items list', 'yawave'),
            'items_list_navigation' => __('Items list navigation', 'yawave'),
        );
        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud' => true,
        );
        register_taxonomy('media_type', array('post'), $args);
    }

    add_action('init', 'Yawave\portal_taxonomy', 0);
}
