<?php
/**
 * This is used to return value of article meta field of post
 * No restrictions needed, this meta is only for publications and always public
 */

function yawave_publication_befree_preview_handler() {
    $id = sanitize_text_field($_GET['id']);
    $content = get_post_meta($id, 'article', true);
    if (!empty($content))
        echo $content;
    wp_die();
}

add_action('wp_ajax_publication', 'Yawave\yawave_publication_befree_preview_handler');
add_action('wp_ajax_nopriv_publication', 'Yawave\yawave_publication_befree_preview_handler');
