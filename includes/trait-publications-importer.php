<?php

namespace Yawave;

trait WP_Yawave_Publications_Importer {

    /**
     * Update publicaiton - main method to fetch publicaiton from API and push into WordPress
     * Tags are paginated import. If more pages - add it to queue
     */
    public function update_publications($page = 0) {

        $url = YAWAVE_API_ENDPOINT_PUBLICATIONS . '&page=' . $page;
        $yawave_publications = $this->get_api_endpoint_data($url);

        if ($page == 0) {
            $this->push_to_queue_other_publications_pages($yawave_publications);
        }

        if ($yawave_publications && isset($yawave_publications->content) && is_array($yawave_publications->content) && sizeof($yawave_publications->content) > 0) {
            foreach ($yawave_publications->content as $publication) {
                $this->save_yawave_publication($publication);
            }
        }
        return true;
    }

    /*
     * return publication by external yawave id
     */

    public function get_wp_publication_by_yawave_id($yawave_id) {
        $args = array(
            'post_type' => 'publication',
            'post_status' => 'publish',
            'numberposts' => 1,
            'meta_query' => array(
                array(
                    'key' => 'yawave_id',
                    'value' => $yawave_id,
                    'compare' => '='
                )
            )
        );
        return get_posts($args);
    }

    /**
     * Add, update or skip publication
     * Update only if checksum is changed
     * @param type $args
     * @param type $publication
     */
    public function save_prepared_wp_post_with_featured_image($args, $publication) {
        $wp_post = $this->get_wp_publication_by_yawave_id($publication->id);
        if ($wp_post && is_array($wp_post) && isset($wp_post[0])) {
            if ($this->is_publication_diff($publication, $wp_post[0]->ID)) {


                $this->log("publication: " . $wp_post[0]->ID . " : " . $publication->cover->title . " is updating...");

                $args['ID'] = $wp_post[0]->ID;
                kses_remove_filters();
                wp_update_post($args);
                kses_init_filters();

                update_post_meta($wp_post[0]->ID, 'yawave_header', $publication->layout->header);
                update_post_meta($wp_post[0]->ID, 'yawave_badge', $publication->layout->badge);
                update_post_meta($wp_post[0]->ID, 'yawave_publication_control_sum', $this->publication_control_sum($publication));
                update_field('type', $publication->type, $wp_post[0]->ID);
                update_field('last_sync', date("Y-m-d H:i:s"), $wp_post[0]->ID);

                if (isset($publication->content->url) && !empty($publication->content->url)) {
                    update_field('video', $publication->content->url, $wp_post[0]->ID);
                }
                if (isset($publication->content->embed_code) && !empty($publication->content->embed_code)) {
                    update_field('embed_code', $publication->content->embed_code, $wp_post[0]->ID);
                }
                if (isset($publication->content->html) && !empty($publication->content->html)) {
                    update_field('article', $publication->content->html, $wp_post[0]->ID);
                }
                if (isset($publication->header->image) && !empty($publication->header->image) && isset($publication->header->image->path)) {
                    update_field('header_image', $publication->header->image->path, $wp_post[0]->ID);
                }
                if (isset($publication->cover->title_image) && isset($publication->cover->title_image->path) && !empty($publication->cover->title_image->path)) {
                    update_field('logo', '<img src="' . $publication->cover->title_image->path . '" alt="' . $publication->cover->title . '" class="publication-logo" />', $wp_post[0]->ID);
                }
                if (isset($publication->header->image) && !empty($publication->header->image) && isset($publication->header->image->focus)) {
                    update_field('focus_x', $publication->header->image->focus->x, $wp_post[0]->ID);
                    update_field('focus_y', $publication->header->image->focus->y, $wp_post[0]->ID);
                }
                if (isset($publication->cover->image) && !empty($publication->cover->image) && isset($publication->cover->image->focus)) {
                    update_field('cover_image', $publication->cover->image->path, $wp_post[0]->ID);
                    update_field('cover_focus_x', $publication->cover->image->focus->x, $wp_post[0]->ID);
                    update_field('cover_focus_y', $publication->cover->image->focus->y, $wp_post[0]->ID);
                }

                $action_buttons = [];
                if (!empty($publication->layout->action_buttons)) {
                    $i = 1;
                    foreach ($publication->layout->action_buttons as $button) {
                        $action_buttons_tmp = [];
                        $action_buttons_tmp['code'] = $button;
                        $action_buttons_tmp['type'] = $i++;
                        $action_buttons[] = $action_buttons_tmp;
                    }
                }
                update_field('action_buttons', $action_buttons, $wp_post[0]->ID);


                if (sizeof($action_buttons) > 0) {
                    update_field('first_button', $action_buttons[0]['code'], $wp_post[0]->ID);
                }

                $this->assign_publication_to_media_type($publication, $wp_post);

                if (isset($publication->main_category_id) && !empty($publication->main_category_id)) {
                    $wp_category = $this->get_category_by_yawave_id($publication->main_category_id);
                    if (!empty($wp_category)) {
                        update_field('main_category', $wp_category->term_id, $wp_post[0]->ID);
                    }
                }

                if (!empty($this->get_value($publication->cover->image)) && $this->is_publication_featured_image_diff($publication, $wp_post[0]->ID)) {
                    $this->log("updating post featured image");
                    $this->upload_and_save_featured_image($this->get_value($publication->cover->image), $wp_post[0]->ID);
                    update_post_meta($wp_post[0]->ID, 'yawave_publication_featured_image_control_sum', $this->publication_control_sum($this->get_value($publication->cover->image)));
                }
            }
        } else {

            kses_remove_filters();
            $wp_post_id = wp_insert_post($args);
            kses_init_filters();

            add_post_meta($wp_post_id, 'yawave_header', $publication->layout->header);
            add_post_meta($wp_post_id, 'yawave_badge', $publication->layout->badge);

            add_post_meta($wp_post_id, 'yawave_id', $publication->id, true);
            add_post_meta($wp_post_id, 'yawave_publication_control_sum', $this->publication_control_sum($publication), true);
            add_post_meta($wp_post_id, 'yawave_publication_featured_image_control_sum', $this->publication_control_sum($this->get_value($publication->cover->image)), true);

            if ($wp_post_id && !empty($this->get_value($publication->cover->image))) {
                $this->upload_and_save_featured_image($this->get_value($publication->cover->image), $wp_post_id);
            }
        }
    }

    /*
     * Depends on type of publicaiton assign it into correct taxonomy
     */

    public function assign_publication_to_media_type($publication, $wp_post) {
        $term = get_term_by('slug', $publication->type, 'media_type');
        if (!$term) {
            $wp_term = wp_insert_term($publication->type, 'media_type');
            $wp_media_type_id = $wp_term['term_id'];
        } else {
            $wp_media_type_id = $term->term_id;
        }
        wp_set_post_terms($wp_post[0]->ID, array($wp_media_type_id), 'media_type');
    }

    /**
     * Save publication as post. Depends on type modify content
     * @param type $publication
     */
    public function save_yawave_publication($publication) {
        $args = $this->get_basic_wp_post_args($publication);
        $tabs = $this->get_value($publication->layout->tabs);
        $content = html_entity_decode($this->get_content_converted_from_tabs($tabs));
        $args['post_content'] = $content;
        if ($publication->type == "ARTICLE" || $publication->type == "NEWSLETTER" || $publication->type == "LANDING_PAGE") {
            $args['post_content'] = $publication->content->html;
        }
        if ($publication->type == "PDF") {
            $content_description = "";
            if (!empty($publication->content->description) && $publication->content->description != $publication->header->description) {
                $content_description = "<p>" . $publication->content->description . "</p>";
            }
            $args['post_content'] = $content_description . '<embed src="' . $publication->content->url . '" type="application/pdf" width="100%" height="1000">';
        }
        $this->save_prepared_wp_post_with_featured_image($args, $publication);
    }

    /**
     * Add tabs for content.
     * @param type $tabs
     * @return string
     */
    public function get_content_converted_from_tabs($tabs) {
        $wp_tabs = [];
        foreach ($tabs as $tab) {
            if (isset($tab->enabled) && $tab->enabled) {
                $wp_tabs[$tab->name] = isset($tab->content) ? $tab->content : "";
            }
        }
        if (sizeof($wp_tabs) > 0) {
            $i = 1;
            $content = '<div id="tabs"><ul>';
            foreach ($wp_tabs as $label => $tab_content) {
                $content .= '<li><a href="#tabs-' . $i . '">' . $label . '</a></li>';
                $i++;
            }
            $content .= '</ul>';
            $i = 1;
            foreach ($wp_tabs as $label => $tab_content) {
                $content .= '<div id="tabs-' . $i . '">' . $tab_content . '</div>';
            }
            $content .= '</div>';
            return $content;
        } else {
            return "";
        }
    }

    /**
     * Prepare standard params for each publication
     * @param type $publication
     * @return type
     */
    public function get_basic_wp_post_args($publication) {
        return array(
            'post_type' => 'publication',
            'post_author' => $this->get_author_id(),
            'post_status' => 'publish',
            'post_date' => date("Y-m-d H:i:s", strtotime($publication->creation_date)),
            'post_date_gmt' => date("Y-m-d H:i:s", get_gmt_from_date($publication->creation_date)),
            'post_title' => $this->get_value($publication->cover->title),
            'post_excerpt' => $this->get_value($publication->cover->description),
            'post_category' => $this->get_assigned_categories_ids($publication->category_ids, $publication->main_category_id),
        );
    }

    /**
     * Assing post to correct taxonomies by ids
     * @param type $yawave_categories_ids
     * @param type $main_category_id
     * @return type
     */
    public function get_assigned_categories_ids($yawave_categories_ids, $main_category_id) {
        if (!empty($main_category_id)) {
            $yawave_categories_ids[] = $main_category_id;
        }

        $wp_categories_ids = [];
        if ($yawave_categories_ids && is_array($yawave_categories_ids) && sizeof($yawave_categories_ids)) {
            foreach ($yawave_categories_ids as $yawave_category_id) {
                $wp_category = $this->get_category_by_yawave_id($yawave_category_id);
                if (!empty($wp_category) && !in_array($wp_category->term_id, $wp_categories_ids)) {
                    $wp_categories_ids[] = $wp_category->term_id;
                }
            }
        }
        return $wp_categories_ids;
    }

    /**
     * Find wordpress taxonomy ids based on yawawve tags
     * @param type $yawave_tags
     * @return type
     */
    public function get_assigned_tags_ids($yawave_tags) {
        $wp_tags_ids = [];
        if ($yawave_tags && is_array($yawave_tags) && sizeof($yawave_tags)) {
            foreach ($yawave_tags as $yawave_tag) {
                $wp_tag = $this->get_tag_by_yawave_id($yawave_tag->id);
                if (!empty($wp_tag)) {
                    $wp_tags_ids[] = $wp_tag->term_id;
                }
            }
        }
        return $wp_tags_ids;
    }

    /**
     * Save uploaded image into media
     * @param type $publication_image
     * @param type $post_id
     * @return boolean
     */
    public function upload_and_save_featured_image($publication_image, $post_id) {
        $url = $publication_image->path;
        $file = array();
        $file['name'] = $url;
        $file['tmp_name'] = download_url($url);

        if (is_wp_error($file['tmp_name'])) {
            @unlink($file['tmp_name']);
            return false; //var_dump( $file['tmp_name']->get_error_messages( ) );
        } else {
            $attachmentId = media_handle_sideload($file, $post_id);
            set_post_thumbnail($post_id, $attachmentId);
            if (is_wp_error($attachmentId)) {
                @unlink($file['tmp_name']);
            }
            return $attachmentId;
        }
    }

    /*
     * Temporary solution - will be fixed with next release of API.
     */

    public function get_url_publication_image($publication_image) {
        $url = false;
        if (!empty($publication_image->path)) {
            if (strpos($publication_image->path, 's3://') >= 0) {
                $url = str_replace('s3://', YAWAVE_S3_URL, $publication_image->path);
            } elseif (strpos($publication_image->path, 'public://') >= 0) {
                $url = str_replace('public://', YAWAVE_PUBLIC_URL, $publication_image->path);
            }
        }
        $url = $this->get_redirect_final_target($url);
        return $url;
    }

    /**
     * Return number of pages of endpoint
     * @param type $tags_object
     * @return integer
     */
    public function get_number_of_publication_pages($tags_object) {
        return (isset($tags_object->number_of_all_pages)) ? $tags_object->number_of_all_pages : 1;
    }

    /**
     * Push to queue another pages
     * @param type $tags_object
     */
    public function push_to_queue_other_publications_pages($tags_object) {
        $pages = $this->get_number_of_publication_pages($tags_object);
        if ($pages > 1) {
            for ($i = 1; $i <= $pages; $i++) {
                $this->push_to_queue("publications_" . $i);
            }
            $this->save();
        }
    }

    public function video_embeded($url) {
        if (strpos($url, '.youtube.') > 0) {
            $url = 'https://www.youtube.com/watch?v=u9-kU7gfuFA';
            preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
            $id = $matches[1];
            return '<iframe id="ytplayer" type="text/html" width="100%" height="500px" src="https://www.youtube.com/embed/' . $id . '?rel=0&showinfo=0&color=white&iv_load_policy=3" frameborder="0" allowfullscreen></iframe>';
        }
    }

    public function link_template($link, $title) {
        return '<a href="' . $link . '" target="_blank">' . $title . '</a>';
    }

    public function photo_template($url, $title) {
        $img_url = $this->get_url_publication_image($url);
        return (!empty($img_url)) ? '<img src="' . $img_url . '" alt="' . $title . '" title="' . $title . '" />' : "";
    }

    public function is_publication_diff($publication, $wp_post_id) {
        $sum = get_post_meta($wp_post_id, 'yawave_publication_control_sum', true);
        return ($this->publication_control_sum($publication) !== $sum);
    }

    public function is_publication_featured_image_diff($publication, $wp_post_id) {
        if (!has_post_thumbnail($wp_post_id)) {
            return true;
        } // if no featured image
        $sum = get_post_meta($wp_post_id, 'yawave_publication_featured_image_control_sum', true);
        return ($this->publication_control_sum($this->get_value($publication->cover->image)) !== $sum);
    }

    public function publication_control_sum($publication) {
        return md5(serialize($publication));
    }

    public function get_redirect_final_target($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // follow redirects
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // set referer on redirect
        curl_exec($ch);
        $target = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
        if ($target)
            return $target;
        return false;
    }

}
