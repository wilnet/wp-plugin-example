<?php

namespace Yawave;

trait WP_Yawave_Importer {

    protected $token = '';
    protected $app_id = '';

    /**
     * This is helper method used in development and debuging
     * reguire activate debug mode in wp-config.php
     *
     * @param string $message
     */
    public function log($message) {
        error_log(print_r($message, true));
    }

    /**
     * 
     */
    public function set_api_token_and_app_id() {
        $auth_options = get_option('yawave_settings_authorization_option');
        if (empty($auth_options) || !is_array($auth_options) || !isset($auth_options['yawave_authorization_key']) || !isset($auth_options['yawave_authorization_secret'])) {
            return false;
        }

        $key = $auth_options['yawave_authorization_key'];
        $secret = $auth_options['yawave_authorization_secret'];


        $data = array(
            'grant_type' => $auth_options['yawave_authorization_appid']
        );

        $additionalHeaders = "";
        $ch = curl_init("https://auth.yawave.com/token");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', $additionalHeaders));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERPWD, $key . ":" . $secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $return = curl_exec($ch);
        $response = json_decode($return);
        if ($response && isset($response->access_token) && !empty($response->access_token)) {
            $this->token = $response->access_token;
        }

        if (empty($auth_options['yawave_authorization_appid'])) {
            $this->app_id = str_replace('"', '', $this->get_api_endpoint_data('https://api.yawave.com/public/applications/id/'));
        } else {
            $this->app_id = $auth_options['yawave_authorization_appid'];
        }

        curl_close($ch);
    }

    /**
     * Return array of data from called API ENDPOINT
     * @todo error handling, add params, check response format and validate
     * 
     * @param type $url
     * @param type $params (optional)
     * @return array (parsed JSON)
     */
    public function get_api_endpoint_data($url_with_placeholder, $params = []) {

        $url = str_replace("YAWAVE_APP_ID", $this->app_id, $url_with_placeholder);
        
        $header = array(
            "Authorization: Bearer {$this->token}",
            'Content-Type: application/json'
        );

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }

    /**
     * Return value of attribute by language, if language not present return in defatult language
     * If attribute not existing in choosen language, return false
     * This method is used for data imported form Yawave platform for multilanguage elements
     * @param type $object
     * @param type $language
     * @return string or false
     */

    public function get_value($object, $language = false) {
        return $object;
    }

    /**
     * Return default WordPress language code mapped to Yawave language codes
     * It's needed in Yawave API to get content
     * @todo Temporary its hardcoded and return "en". 
     * @return string
     */
    public function get_default_language_code() {
        return "en";
    }
    
    /**
     * Return id of user who is set as default author in option page
     * @return type
     */

    public function get_author_id() {
        $options = get_option('yawave_settings_import_option');
        return (isset($options['yawave_import_author_user'])) ? $options['yawave_import_author_user'] : false;
    }

}
