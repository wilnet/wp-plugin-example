<?php

namespace Yawave;

// This function will run once the 'delete_post_revisions' is called
function update_categories() {
    // TODO: after finish development activate synchornisation every hour
}

add_action('wp_cron_jobs_yawave_update', 'Yawave\update_categories');

if (!wp_next_scheduled('wp_cron_jobs_update_categories')) {
    // Schedule the event
    wp_schedule_event(time(), 'hourly', 'wp_cron_jobs_yawave_update');
}
