<?php

namespace Yawave;

class YawaveSettings {

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
    private $activeTab;

    /**
     * Start up
     */
    public function __construct() {
        $this->activeTab();
        add_action('admin_menu', array($this, 'add_settings_page'));
        add_action('admin_init', array($this, 'authorization_tab_init'));
        add_action('admin_init', array($this, 'import_tab_init'));
    }

    /**
     * Add options page
     */
    public function add_settings_page() {
        // This page will be under "Settings"
        add_menu_page(
                'Yawave Settings',
                'Yawave',
                'manage_options',
                'yawave-setting-admin',
                array($this, 'create_settings_page')
        );
    }

    private function activeTab() {
        $this->activeTab = "home-options";
        if (isset($_GET["tab"])) {
            if ($_GET["tab"] == "authorization-options") {
                $this->activeTab = "authorization-options";
            } elseif ($_GET["tab"] == "import-options") {
                $this->activeTab = "import-options";
            } elseif ($_GET["tab"] == "tools-options") {
                $this->activeTab = "tools-options";
            } else {
                $this->activeTab = "home-options";
            }
        }
    }

    /**
     * Options page callback
     */
    public function create_settings_page() {
        ?>

        <div class="wrap">
            <a href="https://yawave.ch/" target="_blank"><img src="<?php echo plugin_dir_url('yawave/assets/img/yawave_logo.png') ?>yawave_logo.png" alt="Yawave" width="80" align="right" /></a>
            <h1>Yawave Settings</h1>
            <h2 class="nav-tab-wrapper">
                <!-- when tab buttons are clicked we jump back to the same page but with a new parameter that represents the clicked tab. accordingly we make it active -->
                <a href="?page=yawave-setting-admin&tab=home-options" class="nav-tab <?php
                if ($this->activeTab == 'home-options') {
                    echo 'nav-tab-active';
                }
                ?> "><?php _e('About Yawave', 'yawave'); ?></a>
                <a href="?page=yawave-setting-admin&tab=authorization-options" class="nav-tab <?php
                if ($this->activeTab == 'authorization-options') {
                    echo 'nav-tab-active';
                }
                ?> "><?php _e('Authorization', 'yawave'); ?></a>
            </h2>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields

                if ($this->activeTab == 'authorization-options') {
                    $this->options = get_option('yawave_settings_authorization_option');
                    settings_fields('yawave_settings_authorization_group');
                    do_settings_sections('yawave-setting-authorization-admin');
                }
                if ($this->activeTab == 'import-options') {
                    $this->options = get_option('yawave_settings_import_option');
                    settings_fields('yawave_settings_import_group');
                    do_settings_sections('yawave-setting-import-admin');
                }

                if ($this->activeTab != 'home-options' && $this->activeTab != 'tools-options') {
                    submit_button();
                }
                ?>
            </form>
            <?php if ($this->activeTab == "tools-options") : ?>
                <?php $this->import_buttons_render(); ?>
            <?php endif; ?>
        </div>
        <?php
    }

    /**
     * Register and add settings for authorization
     */
    public function authorization_tab_init() {
        register_setting(
                'yawave_settings_authorization_group', // Option group
                'yawave_settings_authorization_option', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'authorization_section', // ID
                'Authorization', // Title
                array($this, 'print_authorization_section_info'), // Callback
                'yawave-setting-authorization-admin' // Page
        );

        add_settings_field(
                'yawave_key', // ID
                'API key', // Title 
                array($this, 'yawave_authorization_key_callback'), // Callback
                'yawave-setting-authorization-admin', // Page
                'authorization_section' // Section           
        );

        add_settings_field(
                'yawave_secret',
                'API secret',
                array($this, 'yawave_authorization_secret_callback'),
                'yawave-setting-authorization-admin',
                'authorization_section'
        );

        add_settings_field(
                'yawave_appid', // ID
                'Yawave Application ID', // Title 
                array($this, 'yawave_authorization_appid_callback'), // Callback
                'yawave-setting-authorization-admin', // Page
                'authorization_section' // Section           
        );
    }

    /**
     * Register and add settings for import
     */
    public function import_tab_init() {
        register_setting(
                'yawave_settings_import_group', // Option group
                'yawave_settings_import_option', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'import_section', // ID
                'Import Settings', // Title
                array($this, 'print_import_section_info'), // Callback
                'yawave-setting-import-admin' // Page
        );

        add_settings_field(
                'yawave_import_author_user', // ID
                'Author of imported publications', // Title 
                array($this, 'yawave_import_author_user_callback'), // Callback
                'yawave-setting-import-admin', // Page
                'import_section' // Section     
        );

        add_settings_field(
                'yawave_import_categories', // ID
                'Import categories', // Title 
                array($this, 'yawave_import_categories_callback'), // Callback
                'yawave-setting-import-admin', // Page
                'import_section' // Section     
        );

        add_settings_field(
                'yawave_import_tags', // ID
                'Import tags', // Title 
                array($this, 'yawave_import_tags_callback'), // Callback
                'yawave-setting-import-admin', // Page
                'import_section' // Section     
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input) {

        $new_input = array();
        if (isset($input['id_number'])) {
            $new_input['id_number'] = absint($input['id_number']);
        }

        if (isset($input['title'])) {
            $new_input['title'] = sanitize_text_field($input['title']);
        }

        if (isset($input['yawave_authorization_key'])) {
            $new_input['yawave_authorization_key'] = sanitize_text_field($input['yawave_authorization_key']);
        }

        if (isset($input['yawave_authorization_appid'])) {
            $new_input['yawave_authorization_appid'] = sanitize_text_field($input['yawave_authorization_appid']);
        }

        if (isset($input['yawave_authorization_secret'])) {
            $new_input['yawave_authorization_secret'] = sanitize_text_field($input['yawave_authorization_secret']);
        }

        if (isset($input['yawave_import_tags'])) {
            $new_input['yawave_import_tags'] = ($input['yawave_import_tags'] == "yes") ? "yes" : "no";
        }

        if (isset($input['yawave_import_author_user'])) {
            $new_input['yawave_import_author_user'] = sanitize_text_field($input['yawave_import_author_user']);
        }

        if (isset($input['yawave_import_categories'])) {
            $new_input['yawave_import_categories'] = ($input['yawave_import_categories'] == "yes") ? "yes" : "no";
        }
        return $new_input;
    }

    /**
     * Print the Sections text
     */
    public function print_authorization_section_info() {
        print 'Enter Yawave API creditionas below:';
    }

    public function print_import_section_info() {
        print 'Set configuration for import publication from Yawave:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function yawave_authorization_key_callback() {
        printf(
                '<input type="text" id="yawave_key" name="yawave_settings_authorization_option[yawave_authorization_key]" value="%s" />',
                isset($this->options['yawave_authorization_key']) ? esc_attr($this->options['yawave_authorization_key']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function yawave_authorization_appid_callback() {
        printf(
                '<input type="text" id="yawave_appid" name="yawave_settings_authorization_option[yawave_authorization_appid]" value="%s" />',
                isset($this->options['yawave_authorization_appid']) ? esc_attr($this->options['yawave_authorization_appid']) : ''
        );
    }

    public function yawave_authorization_secret_callback() {
        printf(
                '<input type="password" id="yawave_secret" name="yawave_settings_authorization_option[yawave_authorization_secret]" value="%s" />',
                isset($this->options['yawave_authorization_secret']) ? esc_attr($this->options['yawave_authorization_secret']) : ''
        );
    }

    public function yawave_import_categories_callback() {
        $checked = ( isset($this->options['yawave_import_categories']) && $this->options['yawave_import_categories'] == 'yes') ? 'checked="checked"' : '';
        printf(
                '<input type="hidden" id="yawave_import_categories_no" name="yawave_settings_import_option[yawave_import_categories]" value="no" />'
        );
        printf(
                '<input type="checkbox" id="yawave_import_categories" name="yawave_settings_import_option[yawave_import_categories]" value="yes" ' . $checked . ' />'
        );
    }

    public function yawave_import_tags_callback() {
        $checked = ( isset($this->options['yawave_import_tags']) && $this->options['yawave_import_tags'] == 'yes') ? 'checked="checked"' : '';
        printf(
                '<input type="hidden" id="yawave_import_tags_no" name="yawave_settings_import_option[yawave_import_tags]" value="no" />'
        );
        printf(
                '<input type="checkbox" id="yawave_import_tags" name="yawave_settings_import_option[yawave_import_tags]" value="yes" ' . $checked . ' />'
        );
    }

    public function yawave_import_author_user_callback() {
        $users = get_users();
        $user_id = (isset($this->options['yawave_import_author_user'])) ? $this->options['yawave_import_author_user'] : 0;
        printf('<select id="yawave_import_author_user" name="yawave_settings_import_option[yawave_import_author_user]">');
        foreach ($users as $user) {
            $selected = ($user->data->ID == $user_id) ? ' selected="selected"' : "";
            printf('<option value="' . $user->data->ID . '"' . $selected . '>' . $user->data->user_nicename . '</option>');
        }
        print_r('</select>');
    }

    /**
     * Import buttons
     */
    public function import_buttons_render() {
        ?>


        <h2>Tools</h2>
        <button id="run-import"><?php _e('Press here to import publications', 'yawave') ?></button>
        <script>
            jQuery("button#run-import").click(function () {
                data = {action: 'getpublicaitons', avalue: 'some value', 'anothervalue': 'another value'};
                jQuery.post(ajaxurl, data, function (response) {
                    //alert(response);
                    console.log(response);
                });
            });
        </script>
        <?php
        if (isset($_GET['import']) && $_GET['import'] == 'categories') {
            ?>
            <p>import is running...</p>
            <?php
            //$process_single = new WP_Categories_Request();

            $process_all = new WP_Yawave_Importer_Process();
            $process_all->push_to_queue("publications");
            $process_all->save()->dispatch();
        }
    }

}

if (is_admin()) {
    $my_settings_page = new YawaveSettings();
}