<?php

add_action('wp_ajax_import_categories', 'my_ajax_foobar_handler');

function my_ajax_foobar_handler() {
    // Make your response and echo it.
    // Don't forget to stop execution afterward.
    wp_die();
}
