<?php

namespace Yawave;

trait WP_Yawave_Categories_Importer {

    /**
     * Update categories - main method to fech categories from API and push into WordPress
     * First are imported parent categories, then child categoires 
     */
    public function update_categories($page = 0) {

        $yawave_categories = $this->get_api_endpoint_data(YAWAVE_API_ENDPOINT_CATEGORIES . '&page=' . $page);
        if ($page == 0) {
            $this->push_to_queue_other_categories_pages($yawave_categories);
        }

        if ($yawave_categories && isset($yawave_categories->content) && is_array($yawave_categories->content) && sizeof($yawave_categories->content) > 0) {
            foreach ($yawave_categories->content as $category) {
                if (empty($category->parentId)) {
                    $this->save_category($category);
                }
            }
            foreach ($yawave_categories->content as $category) {
                if (!empty($category->parentId)) {
                    $this->save_category($category);
                }
            }
        }

        return true;
    }

    /**
     * Push to queue another pages
     * @param type $tags_object
     */
    public function push_to_queue_other_categories_pages($categories_object) {
        $pages = $this->get_number_of_categories_pages($categories_object);
        if ($pages > 1) {
            for ($i = 2; $i <= $pages; $i++) {

                $this->push_to_queue("categories_" . $i);
            }
            $this->save();
        }
    }

    /**
     * Return number of pages of endpoint
     * @param type $tags_object
     * @return integer
     */
    public function get_number_of_categories_pages($categories_object) {
        return (isset($categories_object->number_of_all_pages)) ? $categories_object->number_of_all_pages : 1;
    }

    /**
     * Save category into WP
     * @category object $category 
     * stdClass Object
     *  (
     *      [applicationId] => 5bf40b32e7ef860001486041
     *      [id] => 5c3ed1bd66c9d600012f1636
     *      [parentId] => 5c3ed1bd66c9d600012f1635
     *      [name] => Other - Find
     *      [slug] => other-find
     *      [weight] => 6
     *      [active] => 1
     *      [count] => 66
     *  )
     */
    public function save_category($category) {

        $wp_category_id = $this->get_category_by_yawave_id($category->id);

        if (empty($wp_category_id)) {
            $wp_category_id = $this->create_wp_category($category->name, $category->slug, $category->id);
            if (!empty($category->parent_id)) {
                $this->assign_child_category_to_parent($wp_category_id->term_id, $category->parent_id);
            }
            $this->log("category " . $category->name . " created");
        } else {
            $this->log("category " . $category->name . " exist");
            wp_update_term($wp_category_id->term_id, 'category', array('name' => $category->name, 'slug' => $category->slug));

            if (!empty($category->parent_id)) {
                $this->assign_child_category_to_parent($wp_category_id->term_id, $category->parent_id);
            }
        }
        return true;
    }

    /**
     * Create new WordPress category, then adding to metadata yawave id
     * @param type $name
     * @param type $slug
     * @param type $yawave_id
     * @return type
     */
    public function create_wp_category($name, $slug, $yawave_id) {
        $wp_category = array(
            'cat_name' => $name,
            'category_nicename' => $slug,
            'taxonomy' => 'category'
        );
        $wp_category_id = \wp_insert_category($wp_category);
        if ($wp_category_id && !empty($yawave_id)) {
            \add_term_meta($wp_category_id, 'yawave_id', $yawave_id);
        }
        return $wp_category_id;
    }

    /**
     * Find a parent category by Yavawe id and assign it to relevant WordPress category
     * @param type $wp_category_id
     * @param type $yawave_parent_category_id
     */
    public function assign_child_category_to_parent($wp_category_id, $yawave_parent_category_id) {
        $wp_parent_category = $this->get_category_by_yawave_id($yawave_parent_category_id);
        if ($wp_parent_category) {
            \wp_update_category(['cat_ID' => $wp_category_id, 'category_parent' => $wp_parent_category->term_id]);
        }
    }

    /**
     * Finds categories by Yawave ID and return first
     * @param string $yawave_id 
     */
    public function get_category_by_yawave_id($yawave_id) {
        $categories = get_terms(
                array(
                    'taxonomy' => 'category',
                    'hide_empty' => false,
                    'meta_query' => array(
                        array(
                            'key' => 'yawave_id',
                            'value' => $yawave_id,
                            'compare' => '='
                        )
                    )
                )
        );
        return (!empty($categories) && is_array($categories)) ? $categories[0] : false;
    }

}
