<?php

namespace Yawave;

if (!defined('YAWAVE_API_ENDPOINT_CATEGORIES')) {
    define('YAWAVE_API_ENDPOINT_CATEGORIES', 'https://api.yawave.com/public/applications/YAWAVE_APP_ID/categories?lang=en');
}
if (!defined('YAWAVE_API_ENDPOINT_TAGS')) {
    define('YAWAVE_API_ENDPOINT_TAGS', 'https://api.yawave.com/public/applications/YAWAVE_APP_ID/tags?page=0');
}
if (!defined('YAWAVE_API_ENDPOINT_PUBLICATIONS')) {
    define('YAWAVE_API_ENDPOINT_PUBLICATIONS', 'https://api.yawave.com/public/applications/YAWAVE_APP_ID/publications?lang=en');
}
if (!defined('YAWAVE_API_ENDPOINT_PORTALS')) {
    define('YAWAVE_API_ENDPOINT_PORTALS', 'https://api.yawave.com/public/applications/YAWAVE_APP_ID/portals?lang=en');
}