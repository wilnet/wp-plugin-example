<?php

/*
 * Plugin Name: Yawave
 * Plugin URI: https:/yawave.ch/
 * Description: Import publication from Yawave
 * Version: 0.0.2
 * Author: Yawave
 * Text Domain: yawave
 * Author URI: https:/yawave.ch/
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 */

namespace Yawave;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$plugin_version = '0.0.1';
define( 'YAWAVE_VERSION', $plugin_version );

define( 'YAWAVE_ENV', 'DEV' ); // PROD, TEST, DEV

include_once 'includes/widget-recent-posts.php';
include_once 'includes/ajax-calls.php';


class Yawave {

    /**
     * @var WP_Example_Request
     */
    protected $process_single;

    /**
     * @var WP_Example_Process
     */
    protected $process_all;

    /**
     * Loading all dependencies
     * @return void
     */
    public function load() {

        include_once 'config.php';
        include_once 'includes/wp-async-request.php';
        include_once 'includes/wp-background-process.php';

        include_once 'includes/trait-importer.php';
        include_once 'includes/trait-categories-importer.php';
        include_once 'includes/trait-tags-importer.php';
        include_once 'includes/trait-portals-importer.php';
        include_once 'includes/trait-publications-importer.php';
        include_once 'includes/class-importer-request.php';
        include_once 'includes/class-importer-process.php';

        $this->process_single = new WP_Yawave_Importer_Request();
        $this->process_all = new WP_Yawave_Importer_Process();

        include_once 'includes/metadata.php';
        include_once 'includes/settings.php';
    }

}

function yawave_load() {
    $pg = new Yawave();
    $pg->load();
}

// We need to call the function with the namespace
add_action('plugins_loaded', 'Yawave\yawave_load');

// Initialize widgets used on webpage
function yawave_init_widgets() {
    register_widget( 'Yawave\Yawave_Widget_Recent_Posts' );
}
add_action( 'widgets_init', 'Yawave\yawave_init_widgets' );

